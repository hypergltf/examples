-- this (incomplete) hgltf server example was generated with lua5.1 
-- (moonscript Lapis)
-- on an alpine docker container
--
--   packages: apk add lua5.1 luarocks5.1 openresty
--   luarocks: lapis lunajson lrexlib-pcre luautf8
--   Query.lua --> https://gitlab.com/hypergltf/query

local lapis = require("lapis")
local JSON = require("lunajson")
local utils = require("Query") -- see Query.lua @ https://gitlab.com/hypergltf/query
local q = utils["hgltf"]["Query"].new()
do
  local _class_0
  local _parent_0 = lapis.Application
  local _base_0 = {
    ["/"] = function(self)
      return "Welcome to Lapis " .. tostring(require("lapis.version")) .. "!"
    end,
    ["/json"] = function(self)
      local gltfFile = "./example.gltf"
      local f = io.open(gltfFile, "r")
      local gltf = JSON.decode(f:read("*a"))
      f:close()
      local nodes = { }
      local indexes = { }
      local res = { }
      local input = self.GET.q or ""
      local qr = q:parse(input)
      gltf['copy_all'] = qr.copy_all
      for nodekey, node in pairs(gltf.nodes) do
        local j = 0
        local copy = qr.copy_all
        while qr["or"][j] do
          local cond = qr["or"][j]
          if cond["name"] and node.name == cond["name"] then
            copy = true
          end
          if cond["-name"] and node.name == cond["-name"] then
            copy = false
          end
          -- *TODO* check more properties
          j = j + 1
        end
        if copy then
           -- *TODO*: use gltf library to also copy nested objects 
           --         in case of qr.copy_all == false
          table.insert(nodes, node)
          table.insert(indexes, nodekey)
        end
      end
      gltf.scenes = { }
      table.insert(gltf.scenes, {
        name = gltfFile,
        nodes = indexes
      })
      gltf.nodes = nodes
      return JSON.encode(gltf)
    end
  }

  _base_0.__index = _base_0
  setmetatable(_base_0, _parent_0.__base)
  _class_0 = setmetatable({
    __init = function(self, ...)
      return _class_0.__parent.__init(self, ...)
    end,
    __base = _base_0,
    __name = nil,
    __parent = _parent_0
  }, {
    __index = function(cls, name)
      local val = rawget(_base_0, name)
      if val == nil then
        local parent = rawget(cls, "__parent")
        if parent then
          return parent[name]
        end
      else
        return val
      end
    end,
    __call = function(cls, ...)
      local _self_0 = setmetatable({}, _base_0)
      cls.__init(_self_0, ...)
      return _self_0
    end
  })
  _base_0.__class = _class_0
  if _parent_0.__inherited then
    _parent_0.__inherited(_parent_0, _class_0)
  end
  return _class_0
end
