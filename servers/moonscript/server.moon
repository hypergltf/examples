-- this (incomplete) hgltf server example was created with lua5.1 (moonscript Lapis)
-- on an alpine docker container
--
--   packages: apk add lua5.1 luarocks5.1 openresty
--   luarocks: lapis lunajson lrexlib-pcre luautf8
--   Query.lua --> https://gitlab.com/hypergltf/query
--

lapis = require "lapis"
JSON  = require "lunajson"
utils = require "Query"
q     = utils["hgltf"]["Query"].new()

class extends lapis.Application
  "/": =>
    "Welcome to Lapis #{require "lapis.version"}!"
  "/json": =>
	-- load gltf from disk 
	gltfFile = "./example.gltf"
	f        = io.open(gltfFile, "r")
	gltf     = JSON.decode( f\read "*a" )
	f\close()

	nodes = {}
	indexes = {}
	res = {}
	input = @GET.q or ""
	qr    = q\parse input
	gltf['copy_all'] = qr.copy_all
	for nodekey,node in pairs(gltf.nodes)
		j = 0
		copy = qr.copy_all
		while qr.or[j]
			cond = qr.or[j]
			if cond["name"] and node.name == cond["name"]
				copy = true
			if cond["-name"] and node.name == cond["-name"]
				copy = false

			-- *TODO* check more properties

			j = j + 1
		if copy
			-- *TODO*: use gltf library to also copy nested objects 
            --         in case of qr.copy_all == false
			table.insert( nodes, node )
			table.insert( indexes, nodekey)

	gltf.scenes = {}
	table.insert( gltf.scenes, {
		name: gltfFile,
		nodes: indexes
	})
	gltf.nodes = nodes
    JSON.encode gltf
